const getSum = (str1, str2) => {
  if(typeof str1 !== 'string' || typeof str2 !== 'string' || (!Number.isInteger(Number(str1))) || (!Number.isInteger(Number(str2))))
    return false;

  if(str1 === '')
    return str2;

  if(str2 === '')
    return str1;

  if(str2.length > str1.length)
    [str1, str2] = [str2, str1];

  let remainder = 0;
  let resStr = '';

  let j = str2.length - 1;
  for(let i = str1.length - 1; i >=0; i--)
  {
    resStr = ((Number(str1[i]) + Number(str2[j]) + Number(remainder)) % 10) + resStr;
    remainder = Math.floor((Number(str1[i]) + Number(str2[j]) + Number(remainder)) / 10);
    j--;
  }

  return resStr;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let count = 0;
  let commentsCount = 0;
  for(let el of listOfPosts)
  {
    if(el.author === authorName)
      count++;
    if('comments' in el)
      for(let item of el.comments)
        if(item.author === authorName)
          commentsCount++;
      
  }
  return `Post:${count},comments:${commentsCount}`;
}

const tickets=(people)=> {
  let moneyCount = 0;
  for(let person of people)
  {
    moneyCount += 25;
    if(person - 25 - moneyCount > 0)
      return 'NO';
    
    moneyCount -= (person - 25);
  }
  return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
